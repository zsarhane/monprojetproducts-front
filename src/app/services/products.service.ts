import {Injectable} from '@angular/core';
import {IProduct} from '../domain/iproduct';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  constructor(private http: HttpClient) {
  } // DI

  getAllProducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(environment.URL_FAKE);
  }

  getAllProductById(id: string): Observable<IProduct> {
    return this.http.get<IProduct>(environment.URL_FAKE + '/' + id);
  }
}
