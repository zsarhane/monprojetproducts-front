import {Component, OnInit} from '@angular/core';
import {ProductsService} from '../../services/products.service';
import {IProduct} from '../../domain/iproduct';
import {ActivatedRoute} from '@angular/router';
import {activateRoutes} from '@angular/router/src/operators/activate_routes';

@Component({
  selector: 'app-products-detail',
  templateUrl: './products-detail.component.html',
  styleUrls: ['./products-detail.component.scss']
})
export class ProductsDetailComponent implements OnInit {

  id: string;

  iProduct: IProduct;

  constructor(private _service: ProductsService, private _route: ActivatedRoute) {
  }


  ngOnInit() {
    this.id = this._route.snapshot.paramMap.get('id');
    this._service.getAllProductById(this.id).subscribe(resp => this.iProduct = resp,
      error1 => console.log('Attention,  Erreur :' + error1));
  }

  /*getProductDetails(id: string) {
    id = this.id;
    this._service.getAllProductById(id).subscribe(resp => this.iProduct = resp, error1 => console.log('Attention,  Erreur :' + error1));
  }*/

}
